// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';

// void main() => runApp(MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   _MyAppState createState() => _MyAppState();
// }

// var url = "https://jsonplaceholder.typicode.com/photos";
// var data;

// class _MyAppState extends State<MyApp> {
//   @override
//   void initState() {
//     super.initState();
//     fetchData();
//   }

//   fetchData() async {
//     var res = await http.get(url);
//     data = jsonDecode(res.body);
//     setState(() {});
//   }

//   @override
//   void dispose() {
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(title: Text('HTTP')),
//         body: data != null
//             ? ListView.builder(
//                 itemBuilder: (context, index) {
//                   return ListTile(
//                     title: Text(data[index]["title"]),
//                     subtitle: Text("ID: ${data[index]["id"]}"),
//                     leading: Image.network(data[index]["url"]),
//                   );
//                 },
//                 itemCount: data.length,
//               )
//             : Center(
//                 child: CircularProgressIndicator(),
//               ),
//       ),
//     );
//   }
// }

// ----------------------------------------AR-CUSTOM-OBJECT--------------------------------------------------

// import 'dart:typed_data';

// import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:vector_math/vector_math_64.dart' as vector;

// void main() => runApp(MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   _CustomObjectState createState() => _CustomObjectState();
// }

// class _CustomObjectState extends State<MyApp> {
//   ArCoreController arCoreController;

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: const Text('Custom Object on plane detected'),
//         ),
//         body: ArCoreView(
//           onArCoreViewCreated: _onArCoreViewCreated,
//           enableTapRecognizer: true,
//         ),
//       ),
//     );
//   }

//   void _onArCoreViewCreated(ArCoreController controller) {
//     arCoreController = controller;
//     arCoreController.onNodeTap = (name) => onTapHandler(name);
//     arCoreController.onPlaneTap = _handleOnPlaneTap;
//   }

//   Future _addSphere(ArCoreHitTestResult hit) async {
//     final moonMaterial = ArCoreMaterial(color: Colors.grey);

//     final moonShape = ArCoreSphere(
//       materials: [moonMaterial],
//       radius: 0.03,
//     );

//     final moon = ArCoreNode(
//       shape: moonShape,
//       position: vector.Vector3(0.2, 0, 0),
//       rotation: vector.Vector4(0, 0, 0, 0),
//     );

//     final ByteData textureBytes = await rootBundle.load('assets/earth.jpg');

//     final earthMaterial = ArCoreMaterial(
//         color: Color.fromARGB(120, 66, 134, 244),
//         textureBytes: textureBytes.buffer.asUint8List());

//     final earthShape = ArCoreSphere(
//       materials: [earthMaterial],
//       radius: 0.1,
//     );

//     final earth = ArCoreNode(
//         shape: earthShape,
//         children: [moon],
//         position: hit.pose.translation + vector.Vector3(0.0, 1.0, 0.0),
//         rotation: hit.pose.rotation);

//     arCoreController.addArCoreNodeWithAnchor(earth);
//   }

//   void _handleOnPlaneTap(List<ArCoreHitTestResult> hits) {
//     final hit = hits.first;
//     _addSphere(hit);
//   }

//   void onTapHandler(String name) {
//     print("Flutter: onNodeTap");
//     showDialog<void>(
//       context: context,
//       builder: (BuildContext context) =>
//           AlertDialog(content: Text('onNodeTap on $name')),
//     );
//   }

//   @override
//   void dispose() {
//     arCoreController.dispose();
//     super.dispose();
//   }
// }

// ----------------------------------------AR-ASSETS-OBJECT--------------------------------------------------

// import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
// import 'package:flutter/material.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   _AssetsObjectState createState() => _AssetsObjectState();
// }

// class _AssetsObjectState extends State<MyApp> {
//   ArCoreController arCoreController;

//   String objectSelected;

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: const Text('Custom Object on plane detected'),
//         ),
//         body: Stack(
//           children: <Widget>[
//             ArCoreView(
//               onArCoreViewCreated: _onArCoreViewCreated,
//               enableTapRecognizer: true,
//             ),
//             Align(
//               alignment: Alignment.topLeft,
//               child: ListObjectSelection(
//                 onTap: (value) {
//                   objectSelected = value;
//                 },
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   void _onArCoreViewCreated(ArCoreController controller) {
//     arCoreController = controller;
//     arCoreController.onNodeTap = (name) => onTapHandler(name);
//     arCoreController.onPlaneTap = _handleOnPlaneTap;
//   }

//   void _addToucano(ArCoreHitTestResult plane) {
//     if (objectSelected != null) {
//       //"https://github.com/KhronosGroup/glTF-Sample-Models/raw/master/2.0/Duck/glTF/Duck.gltf"
//       final toucanoNode = ArCoreReferenceNode(
//           name: objectSelected,
//           obcject3DFileName: objectSelected,
//           position: plane.pose.translation,
//           rotation: plane.pose.rotation);

//       arCoreController.addArCoreNodeWithAnchor(toucanoNode);
//     } else {
//       showDialog<void>(
//         context: context,
//         builder: (BuildContext context) =>
//             AlertDialog(content: Text('Select an object!')),
//       );
//     }
//   }

//   void _handleOnPlaneTap(List<ArCoreHitTestResult> hits) {
//     final hit = hits.first;
//     _addToucano(hit);
//   }

//   void onTapHandler(String name) {
//     print("Flutter: onNodeTap");
//     showDialog<void>(
//       context: context,
//       builder: (BuildContext context) => AlertDialog(
//         content: Row(
//           children: <Widget>[
//             Text('Remove $name?'),
//             IconButton(
//                 icon: Icon(
//                   Icons.delete,
//                 ),
//                 onPressed: () {
//                   arCoreController.removeNode(nodeName: name);
//                   Navigator.pop(context);
//                 })
//           ],
//         ),
//       ),
//     );
//   }

//   @override
//   void dispose() {
//     arCoreController.dispose();
//     super.dispose();
//   }
// }

// class ListObjectSelection extends StatefulWidget {
//   final Function onTap;

//   ListObjectSelection({this.onTap});

//   @override
//   _ListObjectSelectionState createState() => _ListObjectSelectionState();
// }

// class _ListObjectSelectionState extends State<ListObjectSelection> {
//   List<String> gifs = [
//     "assets/halloween.gif",
//     "assets/TocoToucan.gif",
//     "assets/AndroidRobot.gif",
//     "assets/ArcticFox.gif",
//   ];

//   List<String> objectsFileName = [
//     "halloween.sfb",
//     "toucan.sfb",
//     "andy.sfb",
//     "artic_fox.sfb"
//   ];

//   String selected;

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 150.0,
//       child: ListView.builder(
//         itemCount: gifs.length,
//         scrollDirection: Axis.horizontal,
//         itemBuilder: (context, index) {
//           return GestureDetector(
//             onTap: () {
//               setState(() {
//                 selected = gifs[index];
//                 widget.onTap(objectsFileName[index]);
//               });
//             },
//             child: Card(
//               elevation: 4.0,
//               shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(
//                   Radius.circular(10),
//                 ),
//               ),
//               child: Container(
//                 color:
//                     selected == gifs[index] ? Colors.red : Colors.transparent,
//                 padding: selected == gifs[index] ? EdgeInsets.all(8.0) : null,
//                 child: Image.asset(gifs[index]),
//               ),
//             ),
//           );
//         },
//       ),
//     );
//   }
// }

// -----------------------------------------AR-MATRIX---------------------------------------------------------

// import 'dart:math';

// import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
// import 'package:flutter/material.dart';
// import 'package:vector_math/vector_math_64.dart' as vector;

// void main() => runApp(MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   _Matrix3DRenderingPageState createState() => _Matrix3DRenderingPageState();
// }

// class _Matrix3DRenderingPageState extends State<MyApp> {
//   ArCoreController arCoreController;

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: Scaffold(
//         appBar: AppBar(
//           title: const Text('Hello World'),
//         ),
//         body: ArCoreView(
//           onArCoreViewCreated: _onArCoreViewCreated,
//           enableTapRecognizer: true,
//         ),
//       ),
//     );
//   }

//   void _onArCoreViewCreated(ArCoreController controller) {
//     arCoreController = controller;
//     arCoreController.onNodeTap = (name) => onTapHandler(name);
//     arCoreController.onPlaneTap = _handleOnPlaneTap;
//   }

//   @override
//   void dispose() {
//     arCoreController.dispose();
//     super.dispose();
//   }

//   void _handleOnPlaneTap(List<ArCoreHitTestResult> hits) {
//     final hit = hits.first;
//     _addMatrix3D(arCoreController, hit);
//   }

//   void onTapHandler(String name) {
//     print("Flutter: onNodeTap");
//     showDialog<void>(
//       context: context,
//       builder: (BuildContext context) =>
//           AlertDialog(content: Text('onNodeTap on $name')),
//     );
//   }

//   void _addMatrix3D(
//       ArCoreController arCoreController, ArCoreHitTestResult hit) {
//     final List<ArCoreNode> list = [];
//     for (int i = 0; i < 8; i++) {
//       for (int z = 0; z < 8; z++) {
//         list.add(createNode(createCube(), i, z));
//       }
//     }

//     final node = ArCoreNode(
//       shape: null,
//       position: hit.pose.translation + vector.Vector3(0.0, 0.5, 0.0),
//       rotation: hit.pose.rotation,
//       children: list,
//     );

//     arCoreController.addArCoreNodeWithAnchor(node);
//   }

//   createNode(ArCoreCube shape, int i, int z) {
//     final cubeNode = ArCoreNode(
//       shape: shape,
//       position: vector.Vector3(0.1 * z, 0.0, -0.1 * i),
//     );

//     return cubeNode;
//   }

//   createCube() {
//     final material = ArCoreMaterial(
//       color: Color.fromARGB(255, Random().nextInt(255), Random().nextInt(255),
//           Random().nextInt(255)),
//       metallic: 1.0,
//     );
//     final cube = ArCoreCube(
//       materials: [material],
//       size: vector.Vector3(0.1, 0.1, 0.1),
//     );

//     return cube;
//   }
// }

// -------------------------------------------CHAT-FIREBASE--------------------------------------------------------

// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//         debugShowCheckedModeBanner: false,
//         title: 'FLUTTER CHAT',
//         theme: ThemeData.dark(),
//         initialRoute: MyHomePage.id,
//         routes: {
//           MyHomePage.id: (context) => MyHomePage(),
//           Registration.id: (context) => Registration(),
//           Login.id: (context) => Login(),
//           Chat.id: (context) => Chat(),
//         });
//   }
// }

// class MyHomePage extends StatelessWidget {
//   static const String id = "HOMESCREEN";
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         crossAxisAlignment: CrossAxisAlignment.center,
//         children: <Widget>[
//           Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: <Widget>[
//                 Hero(
//                   tag: 'logo',
//                   child: Container(
//                       width: 100.0,
//                       child: Image.asset(
//                         'assets/game.png',
//                         height: 40,
//                         width: 40,
//                         color: Colors.white,
//                       )),
//                 ),
//                 Text(
//                   'Flutter Chat',
//                   style: TextStyle(
//                     fontSize: 40.0,
//                   ),
//                 ),
//               ]),
//           SizedBox(
//             height: 50.0,
//           ),
//           CustomButton(
//             text: 'Log In',
//             callback: () {
//               Navigator.of(context).pushNamed(Login.id);
//             },
//           ),
//           CustomButton(
//             text: 'Register',
//             callback: () {
//               Navigator.of(context).pushNamed(Registration.id);
//             },
//           )
//         ],
//       ),
//     );
//   }
// }

// class CustomButton extends StatelessWidget {
//   final VoidCallback callback;
//   final String text;

//   const CustomButton({Key key, this.callback, this.text}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: const EdgeInsets.all(8.0),
//       child: Material(
//         color: Colors.blueGrey,
//         elevation: 6.0,
//         borderRadius: BorderRadius.circular(30.0),
//         child: MaterialButton(
//           onPressed: callback,
//           minWidth: 200.0,
//           height: 45.0,
//           child: Text(text),
//         ),
//       ),
//     );
//   }
// }

// class Registration extends StatefulWidget {
//   static const String id = "REGISTRATION";
//   @override
//   _RegistrationState createState() => _RegistrationState();
// }

// class _RegistrationState extends State<Registration> {
//   String email;
//   String password;

//   final FirebaseAuth _auth = FirebaseAuth.instance;

//   Future<void> registerUser() async {
//     try {
//       AuthResult result = await _auth.createUserWithEmailAndPassword(
//           email: email, password: password);
//       FirebaseUser user = result.user;
//       Navigator.push(
//           context,
//           MaterialPageRoute(
//             builder: (context) => Chat(user: user),
//           ));
//     } catch (e) {
//       print(e);
//       return null;
//     }
//   }

//   // Future<void> registerUser() async {
//   //   FirebaseUser user = await _auth.createUserWithEmailAndPassword(
//   //     email: email,
//   //     password: password,
//   //   );

//   //   Navigator.push(
//   //       context,
//   //       MaterialPageRoute(
//   //         builder: (context) => Chat(user: user),
//   //       ));
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Flutter Chat"),
//       ),
//       body: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           children: <Widget>[
//             Expanded(
//                 child: Hero(
//               tag: 'logo',
//               child: Container(
//                   child: Center(
//                 child: Image.asset(
//                   'assets/game.png',
//                   width: 100,
//                   height: 100,
//                 ),
//               )),
//             )),
//             SizedBox(
//               height: 40.0,
//             ),
//             TextField(
//               keyboardType: TextInputType.emailAddress,
//               onChanged: (value) => email = value,
//               decoration: InputDecoration(
//                 hintText: "Enter Your Email...",
//                 border: const OutlineInputBorder(),
//               ),
//             ),
//             SizedBox(
//               height: 40.0,
//             ),
//             TextField(
//               autocorrect: false,
//               obscureText: true,
//               onChanged: (value) => password = value,
//               decoration: InputDecoration(
//                 hintText: "Enter Your Password...",
//                 border: const OutlineInputBorder(),
//               ),
//             ),
//             CustomButton(
//               text: 'Register',
//               callback: () async {
//                 await registerUser();
//               },
//             )
//           ]),
//     );
//   }
// }

// class Login extends StatefulWidget {
//   static const String id = "LOGIN";
//   @override
//   _LoginState createState() => _LoginState();
// }

// class _LoginState extends State<Login> {
//   String email;
//   String password;

//   final FirebaseAuth _auth = FirebaseAuth.instance;

//   Future<void> loginUser() async {
//     try {
//       AuthResult result = await _auth.signInWithEmailAndPassword(
//           email: email, password: password);
//       FirebaseUser user = result.user;
//       Navigator.push(
//           context,
//           MaterialPageRoute(
//             builder: (context) => Chat(user: user),
//           ));
//     } catch (e) {
//       print(e);
//       return null;
//     }
//   }

//   //   Future<void> loginUser() async {
//   //   FirebaseUser user = await _auth.signInWithEmailAndPassword(
//   //     email: email,
//   //     password: password,
//   //   );

//   //   Navigator.push(
//   //       context,
//   //       MaterialPageRoute(
//   //         builder: (context) => Chat(user: user),
//   //       ));
//   // }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Flutter Chat"),
//       ),
//       body: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           children: <Widget>[
//             Expanded(
//                 child: Hero(
//               tag: 'logo',
//               child: Container(
//                   child: Center(
//                 child: Image.asset(
//                   'assets/game.png',
//                   width: 100,
//                   height: 100,
//                 ),
//               )),
//             )),
//             SizedBox(
//               height: 40.0,
//             ),
//             TextField(
//               keyboardType: TextInputType.emailAddress,
//               onChanged: (value) => email = value,
//               decoration: InputDecoration(
//                 hintText: "Enter Your Email...",
//                 border: const OutlineInputBorder(),
//               ),
//             ),
//             SizedBox(
//               height: 40.0,
//             ),
//             TextField(
//               autocorrect: false,
//               obscureText: true,
//               onChanged: (value) => password = value,
//               decoration: InputDecoration(
//                 hintText: "Enter Your Password...",
//                 border: const OutlineInputBorder(),
//               ),
//             ),
//             CustomButton(
//               text: 'Login',
//               callback: () async {
//                 await loginUser();
//               },
//             )
//           ]),
//     );
//   }

// }

// class Chat extends StatefulWidget {
//   static const String id = "CHAT";
//   final FirebaseUser user;

//   const Chat({Key key, this.user}) : super(key: key);
//   @override
//   _ChatState createState() => _ChatState();
// }

// class _ChatState extends State<Chat> {
//   final FirebaseAuth _auth = FirebaseAuth.instance;
//   final Firestore _firestore = Firestore.instance;

//   TextEditingController messageController = TextEditingController();
//   ScrollController scrollController = ScrollController();

//   Future<void> callback() async {
//     if (messageController.text.length > 0) {
//       await _firestore.collection('messages').add({
//         'text': messageController.text,
//         'from': widget.user.email,
//       });
//       messageController.clear();
//         scrollController.animateTo(scrollController.position.maxScrollExtent,
//         duration: const Duration(milliseconds: 300),
//         curve: Curves.easeOut);
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         leading: Hero(
//           tag: 'logo',
//           child: Container(
//             height: 40.0,
//             child: Image.asset(
//               'assets/game.png',
//               width: 20,
//               height: 20,
//             ),
//           ),
//         ),
//         title: Text('Flutter Chat'),
//         actions: <Widget>[
//           IconButton(
//               icon: Icon(Icons.close),
//               onPressed: () {
//                 _auth.signOut();
//                 Navigator.of(context).popUntil((route) => route.isFirst);
//               })
//         ],
//       ),
//       body: SafeArea(
//           child: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: <Widget>[
//           Expanded(
//             child: StreamBuilder<QuerySnapshot>(
//               stream: _firestore.collection('messages').snapshots(),
//               builder: (context, snapshot) {
//                 if (!snapshot.hasData)
//                   return Center(child: CircularProgressIndicator());

//                   List<DocumentSnapshot> docs = snapshot.data.documents;

//                   List<Widget> messages = docs
//                       .map((doc) => Message(
//                             from: doc.data['from'],
//                             text: doc.data['text'],
//                             me: widget.user.email == doc.data['from'],
//                           ))
//                       .toList();

//                 return ListView(
//                   controller: scrollController,
//                   children: <Widget>[
//                     ...messages
//                   ],
//                 );
//               },
//             ),
//           ),
//           Container(
//             child: Row(
//               children: <Widget>[
//                 Expanded(
//                   child: TextField(
//                     onSubmitted: (value) => callback(),
//                     decoration: InputDecoration(
//                       hintText: "Enter a Message...",
//                       border: const OutlineInputBorder(),
//                     ),
//                     controller: messageController,
//                   ),
//                 ),
//                 SendButton(
//                   text: "Send",
//                   callback: callback,
//                 )
//               ],
//             ),
//           )
//         ],
//       )),
//     );
//   }
// }

// class SendButton extends StatelessWidget {
//   final String text;
//   final VoidCallback callback;

//   const SendButton({Key key, this.text, this.callback}) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return FlatButton(
//       color: Colors.orange,
//       onPressed: callback,
//       child: Text(text),
//     );
//   }
// }

// class Message extends StatelessWidget {
//   final String from;
//   final String text;

//   final bool me;

//   const Message({Key key, this.from, this.text, this.me}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//           crossAxisAlignment:
//               me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
//           children: <Widget>[
//             Text(from),
//             Material(
//               color: me ? Colors.teal : Colors.red,
//               borderRadius: BorderRadius.circular(10.0),
//               elevation: 6.0,
//               child: Container(
//                 padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
//                 child: Text(
//                   text,
//                 ),
//               ),
//             )
//           ]),
//     );
//   }
// }

// ----------------------------------------CHAT-FIREBASE-2---------------------------------------------------

import 'package:cloud_firestore/cloud_firestore.dart'; // firebase -> database -> add collection 'messages', rules -> write: if true
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: MyHomePage.id,
      routes: {
        MyHomePage.id: (context) => MyHomePage(),
        Registration.id: (context) => Registration(),
        Login.id: (context) => Login(),
        Chat.id: (context) => Chat(),
      },
    );
  }
}

class MyHomePage extends StatelessWidget {
  static const String id = "HOMESCREEN";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange[900],
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Hero(
                tag: 'logo',
                child: Container(
                  width: 100.0,
                  child: Image.asset(
                    "assets/game.png",
                    width: 40,
                    height: 40,
                  ),
                ),
              ),
              Text(
                "Flutter Chat",
                style: TextStyle(fontSize: 40.0, color: Colors.white),
              ),
            ],
          ),
          SizedBox(
            height: 50.0,
          ),
          CustomButton(
            text: "Log In",
            callback: () {
              Navigator.of(context).pushNamed(Login.id);
            },
          ),
          CustomButton(
            text: "Register",
            callback: () {
              Navigator.of(context).pushNamed(Registration.id);
            },
          )
        ],
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final VoidCallback callback;
  final String text;

  const CustomButton({Key key, this.callback, this.text}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        color: Colors.green,
        elevation: 6.0,
        borderRadius: BorderRadius.circular(30.0),
        child: MaterialButton(
          onPressed: callback,
          minWidth: 200.0,
          height: 45.0,
          child: Text(text),
        ),
      ),
    );
  }
}

class Registration extends StatefulWidget {
  static const String id = "REGISTRATION";
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  String email;
  String password;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<void> registerUser() async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Chat(user: user),
          ));
    } catch (e) {
      print(e);
      return null;
    }
  }

  // Future<void> registerUser() async {
  //   FirebaseUser user = await _auth.createUserWithEmailAndPassword(
  //     email: email,
  //     password: password,
  //   );

  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => Chat(
  //         user: user,
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    var _blankFocusNode = new FocusNode();

    return Scaffold(
      backgroundColor: Colors.orange[900],
      appBar: AppBar(
        title: Text("Flutter Chat"),
        backgroundColor: Colors.orange[900],
        elevation: 0.0,
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(_blankFocusNode);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Hero(
                tag: 'logo',
                child: Container(
                  child: Image.asset(
                    "assets/AndroidRobot.gif",
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            TextField(
              keyboardType: TextInputType.emailAddress,
              onChanged: (value) => email = value,
              decoration: InputDecoration(
                hintText: "Enter Your Email...",
                border: const OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            TextField(
              autocorrect: false,
              obscureText: true,
              onChanged: (value) => password = value,
              decoration: InputDecoration(
                hintText: "Enter Your Password...",
                border: const OutlineInputBorder(),
              ),
            ),
            CustomButton(
              text: "Register",
              callback: () async {
                await registerUser();
              },
            )
          ],
        ),
      ),
    );
  }
}

class Login extends StatefulWidget {
  static const String id = "LOGIN";
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String email;
  String password;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<void> loginUser() async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      FirebaseUser user = result.user;
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Chat(user: user),
          ));
    } catch (e) {
      print(e);
      return null;
    }
  }

  // Future<void> loginUser() async {
  //   FirebaseUser user = await _auth.signInWithEmailAndPassword(
  //     email: email,
  //     password: password,
  //   );

  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => Chat(
  //         user: user,
  //       ),
  //     ),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    var _blankFocusNode = new FocusNode();

    return Scaffold(
      backgroundColor: Colors.orange[900],
      appBar: AppBar(
        title: Text("Flutter Chat"),
        backgroundColor: Colors.orange[900],
        elevation: 0.0,
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(_blankFocusNode);
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: Hero(
                tag: 'logo',
                child: Container(
                  child: Image.asset(
                    "assets/halloween.gif",
                    width: 50,
                    height: 50,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            TextField(
              keyboardType: TextInputType.emailAddress,
              onChanged: (value) => email = value,
              decoration: InputDecoration(
                hintText: "Enter Your Email...",
                border: const OutlineInputBorder(),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            TextField(
              autocorrect: false,
              obscureText: true,
              onChanged: (value) => password = value,
              decoration: InputDecoration(
                hintText: "Enter Your Password...",
                border: const OutlineInputBorder(),
              ),
            ),
            CustomButton(
              text: "Log In",
              callback: () async {
                await loginUser();
              },
            )
          ],
        ),
      ),
    );
  }
}

class Chat extends StatefulWidget {
  static const String id = "CHAT";
  final FirebaseUser user;

  const Chat({Key key, this.user}) : super(key: key);
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;

  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();

  Future<void> callback() async {
    if (messageController.text.length > 0) {
      await _firestore.collection('messages').add({
        'text': messageController.text,
        'from': widget.user.email,
        'date': DateTime.now().toIso8601String().toString(),
      });
      messageController.clear();
      scrollController.animateTo(
        scrollController.position.maxScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    var _blankFocusNode = new FocusNode();

    return Scaffold(
      backgroundColor: Colors.orange[900],
      appBar: AppBar(
        backgroundColor: Colors.orange[900],
        elevation: 0.0,
        leading: Hero(
          tag: 'logo',
          child: Container(
            height: 40.0,
            child: Image.asset("assets/TocoToucan.gif"),
          ),
        ),
        title: Text("Flutter Chat"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              _auth.signOut();
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          )
        ],
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(_blankFocusNode);
        },
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: StreamBuilder<QuerySnapshot>(
                  stream: _firestore
                      .collection('messages')
                      .orderBy('date')
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: CircularProgressIndicator(),
                      );

                    List<DocumentSnapshot> docs = snapshot.data.documents;

                    List<Widget> messages = docs
                        .map((doc) => Message(
                              from: doc.data['from'],
                              text: doc.data['text'],
                              me: widget.user.email == doc.data['from'],
                            ))
                        .toList();

                    return ListView(
                      controller: scrollController,
                      children: <Widget>[
                        ...messages,
                      ],
                    );
                  },
                ),
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        onSubmitted: (value) => callback(),
                        decoration: InputDecoration(
                          hintText: "Enter a Message...",
                          border: const OutlineInputBorder(),
                        ),
                        controller: messageController,
                      ),
                    ),
                    SendButton(
                      text: "Send",
                      callback: callback,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SendButton extends StatelessWidget {
  final String text;
  final VoidCallback callback;

  const SendButton({Key key, this.text, this.callback}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: Colors.orange,
      onPressed: callback,
      child: Text(text),
    );
  }
}

class Message extends StatelessWidget {
  final String from;
  final String text;

  final bool me;

  const Message({Key key, this.from, this.text, this.me}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment:
            me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            from,
          ),
          Material(
            color: me ? Colors.green : Colors.blue,
            borderRadius: BorderRadius.circular(10.0),
            elevation: 6.0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Text(
                text,
              ),
            ),
          )
        ],
      ),
    );
  }
}
